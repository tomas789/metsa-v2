﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.IO;

namespace Metsa
{
    public class TransportationCost
    {
        public MetsaState State;

        public Dictionary<Tuple<Location, Location, Material>, double> InternalShipmentCache = new Dictionary<Tuple<Location, Location, Material>, double>();
        public Dictionary<Tuple<Location, Customer, Material>, double> ExternalShipmentCache = new Dictionary<Tuple<Location, Customer, Material>, double>();
        public Dictionary<Tuple<Location, String, Boolean>, double> ExternalShipmentApproximatedCache = new Dictionary<Tuple<Location, String, Boolean>, double>();
        public Dictionary<Tuple<Location, String, Boolean>, double> InternalShipmentApproximatedCache = new Dictionary<Tuple<Location, String, Boolean>, double>();

        public Dictionary<Location, List<FlowsRow>> RowsByLocation = null;

        public List<FlowsRow> GetRowsByLocation(Location location)
        {
            if (RowsByLocation == null)
            {
                RowsByLocation = new Dictionary<Location, List<FlowsRow>>();
                foreach (var row in State.Flows.Rows)
                {
                    if (!RowsByLocation.ContainsKey(row.ShipFromLoaded))
                        RowsByLocation.Add(row.ShipFromLoaded, new List<FlowsRow>());
                    RowsByLocation[row.ShipFromLoaded].Add(row);
                }
            }

            List<FlowsRow> rows;
            return RowsByLocation.TryGetValue(location, out rows) ? rows : null;
        }

        public List<double> Costs()
        {
            var costs = new List<double>();

            foreach (var item in InternalShipmentCache)
                costs.Add(item.Value);

            foreach (var item in ExternalShipmentCache)
                costs.Add(item.Value);

            return costs;
        }

        public double InternalShipmentPerKg(Location shipFrom, Location shipTo, Material material)
        {
            double cost;
            bool approximation = false;

            if (InternalShipmentCache.TryGetValue(Tuple.Create(shipFrom, shipTo, material), out cost))
                return cost;

            var rows = from r in GetRowsByLocation(shipFrom)
                       where r.ShipToLocationLoaded == shipTo && r.Material == material
                       select r;

            if (!rows.Any())
            {
                if (InternalShipmentApproximatedCache.TryGetValue(Tuple.Create(shipFrom, getRegion(shipTo), material.IsBasepaper), out cost))
                    return cost;

                approximation = true;
                rows = from r in GetRowsByLocation(shipFrom)
                       where r.Inco != "EXW" && r.Inco != "FCA" && r.ShipToLocationLoaded != null &&
                            r.Country == getRegion(shipTo) && r.Material.IsBasepaper == material.IsBasepaper
                       select r;
            }

            var shipmentCostSum = rows.Select(r => r.ShipmentCost).Sum();
            var totalAmount = rows.Select(r => r.Kg).Sum();

            cost = shipmentCostSum / totalAmount;

            if (double.IsInfinity(cost) || double.IsNaN(cost))
            {
                if (shipFrom.ConvertingLines.Count == 0)
                {
                    var internalLocation = State.GetInternalExternalLocationCounterpart(shipFrom);
                    if (internalLocation != null && internalLocation.ConvertingLines.Count != 0)
                        cost = InternalShipmentPerKg(internalLocation, shipTo, material);
                }
            }

            if (shipFrom != shipTo)
                cost = Math.Max(cost, 10e-6);

            InternalShipmentCache.Add(Tuple.Create(shipFrom, shipTo, material), cost);
            if (approximation) InternalShipmentApproximatedCache.Add(Tuple.Create(shipFrom, getRegion(shipTo), material.IsBasepaper), cost);
            return cost;
        }

        public string getRegion(Location location)
        {

            switch (location.PlantCode.Substring(0, 2))
            {
                case "96":
                    return "DE";
                case "79":
                    return "SK";
                case "43":
                    return "PL";
                default: throw new InvalidDataException("Unknown region");
            }
        }

        public double ExternalShipmentPerKg(Location shipFrom, Customer shipTo, Material material)
        {
            double cost;
            bool approximation = false;

            if (ExternalShipmentCache.TryGetValue(Tuple.Create(shipFrom, shipTo, material), out cost))
                return cost;

            var rows = from r in GetRowsByLocation(shipFrom)
                                    where r.ShipToCustomerLoaded == shipTo && r.Material == material
                                    select r;

            if (!rows.Any())
            {
                if (ExternalShipmentApproximatedCache.TryGetValue(Tuple.Create(shipFrom, shipTo.Region, material.IsBasepaper), out cost))
                    return cost;

                approximation = true;
                rows = from r in GetRowsByLocation(shipFrom)
                        where r.Inco != "EXW" && r.Inco != "FCA" && r.ShipToCustomerLoaded != null &&
                            r.Country == shipTo.Region && r.Material.IsBasepaper == material.IsBasepaper
                        select r;
            }

            var shipmentCostSum = rows.Select(r => r.ShipmentCost).Sum();
            var totalAmount = rows.Select(r => r.Kg).Sum();

            cost = shipmentCostSum / totalAmount;

            if (double.IsInfinity(cost) || double.IsNaN(cost))
            {
                if (shipFrom.ConvertingLines.Count == 0)
                {
                    var internalLocation = State.GetInternalExternalLocationCounterpart(shipFrom);
                    if (internalLocation != null && internalLocation.ConvertingLines.Count != 0)
                        cost = ExternalShipmentPerKg(internalLocation, shipTo, material);
                }
            }

            ExternalShipmentCache.Add(Tuple.Create(shipFrom, shipTo, material), cost);
            if (approximation) ExternalShipmentApproximatedCache.Add(Tuple.Create(shipFrom, shipTo.Region, material.IsBasepaper),cost);
            return cost;
        }
    }
}
