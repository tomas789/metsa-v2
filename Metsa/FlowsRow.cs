﻿using System;
using System.Globalization;

namespace Metsa
{
    public class FlowsRow
    {
        public string StorageId;
        public string StorageName;
        public string ShipTo;
        public string Customer;
        public string Country;
        public string PostalCode;
        public string CityOfShipToPart;
        public Material Material;
        public string Inco;
        public string SourceName;
        public long Kg;
        public long Tu;
        public long Du;
        public long Lu;
        public double ShipmentCost;

        public SourceLocator SourceLocator;
        public string MatchingMessage;

        public Location ShipFromLoaded = null;
        public Location ShipToLocationLoaded = null;
        public Customer ShipToCustomerLoaded = null;

        private FlowsRow() { }

        public static CultureInfo ParsingCulture = new CultureInfo("cs-CZ");

        public static bool TryParse(MetsaState state, string row, out FlowsRow result)
        {
            result = new FlowsRow();

            var parts = row.Split(new[] { ';' });
            if (parts.Length != 15)
                return false;

            var material = state.GetMaterial(parts[7]);

            result.StorageId = parts[0];
            result.StorageName = parts[1];
            result.ShipTo = parts[2];
            result.Customer = parts[3];
            result.Country = parts[4];
            result.PostalCode = parts[5];
            result.CityOfShipToPart = parts[6];
            result.Material = material.IsBasepaper ? state.GetBasepaperMixMaterial() : material;
            result.Inco = parts[8];
            result.SourceName = parts[9];

            if (!long.TryParse(parts[10], System.Globalization.NumberStyles.Float, ParsingCulture, out result.Kg)) return false;
            if (!long.TryParse(parts[11], System.Globalization.NumberStyles.Float, ParsingCulture, out result.Tu)) return false;
            if (!long.TryParse(parts[12], System.Globalization.NumberStyles.Float, ParsingCulture, out result.Du)) return false;
            if (!long.TryParse(parts[13], System.Globalization.NumberStyles.Float, ParsingCulture, out result.Lu)) return false;

            if (!double.TryParse(parts[14], System.Globalization.NumberStyles.Float, ParsingCulture, out result.ShipmentCost)) return false;

            return true;
        }
    }
}
