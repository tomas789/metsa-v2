﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using QuickGraph;

namespace Metsa
{
    public class Location : MetsaObjectWithState
    {
        public string PlantCode;
        public string PlantName;
        public string PostalCode;
        public bool HasPaperMill = false;
        public double Latitude;
        public double Longitude;
        public long BasepaperWarehousesOverallCapacity = Tag.InfiniteFlow;
        public long FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow;

        public Location(MetsaState state) : base(state)
        {
        }

        public bool HasBasepaperWarehouse
        {
            get { return BasepaperWarehouseNames != null && BasepaperWarehouseNames.Count != 0; }
        }

        public bool HasFinishedGoodsWarehouse
        {
            get { return FinishedGoodsWarehouseNames != null && FinishedGoodsWarehouseNames.Count != 0; }
        }

        public List<ConvertingLine> ConvertingLines = new List<ConvertingLine>(); 
        

        public List<string> BasepaperWarehouseNames = null;
        public List<string> FinishedGoodsWarehouseNames = null; 

        public BasepaperWarehouse BasepaperWarehouse;
        public Dictionary<Material, FinishedGoodsWarehouse> FinishedGoodsWarehouses = new Dictionary<Material, FinishedGoodsWarehouse>();

        public BasepaperWarehouse GetBasepaperWarehouse()
        {
            if (!HasBasepaperWarehouse)
                return null;

            return BasepaperWarehouse ?? (BasepaperWarehouse = new BasepaperWarehouse(State, this) {Capacity = BasepaperWarehousesOverallCapacity});
        }

        public FinishedGoodsWarehouse GetFinishedGoodsWarehouse(Material material)
        {
            if (material.IsBasepaper)
                Console.WriteLine("Adding basepaper as finished goods");

            if (!HasFinishedGoodsWarehouse)
                return null;
            
            if (!FinishedGoodsWarehouses.ContainsKey(material))
                FinishedGoodsWarehouses.Add(material, new FinishedGoodsWarehouse(State, material, this));

            return FinishedGoodsWarehouses[material];
        }

        public Dictionary<Location, Dictionary<Material, long>> InFlow = new Dictionary<Location, Dictionary<Material, long>>();
        public Dictionary<Location, Dictionary<Material, long>> OutFlow = new Dictionary<Location, Dictionary<Material, long>>();
        public Dictionary<Customer, Dictionary<Material, long>> ToCustomerFlow = new Dictionary<Customer, Dictionary<Material, long>>();

        public Dictionary<Location, Dictionary<Material, long>> SolvedInFlow = new Dictionary<Location, Dictionary<Material, long>>();
        public Dictionary<Location, Dictionary<Material, long>> SolvedOutFlow = new Dictionary<Location, Dictionary<Material, long>>();
        public Dictionary<Customer, Dictionary<Material, long>> SolvedToCustomerFlow = new Dictionary<Customer, Dictionary<Material, long>>();

        public void AddInternalsToGraph(Graph graph)
        {
            if (HasBasepaperWarehouse)
                GetBasepaperWarehouse().AddInternalsToGraph(graph);

            foreach (var convertingLine in ConvertingLines)
                convertingLine.AddInternalsToGraph(graph);

            if (HasFinishedGoodsWarehouse)
            {
                foreach (var kv in InFlow.Values)
                    foreach (var material in kv.Keys.Where(mat => mat.IsFinishedGood))
                        GetFinishedGoodsWarehouse(material);

                foreach (var kv in OutFlow.Values)
                    foreach (var material in kv.Keys.Where(mat => mat.IsFinishedGood))
                        GetFinishedGoodsWarehouse(material);

                foreach (var kv in ToCustomerFlow.Values)
                    foreach (var material in kv.Keys.Where(mat => mat.IsFinishedGood))
                        GetFinishedGoodsWarehouse(material);

                foreach (var convertingLine in ConvertingLines)
                    foreach (var materialNodeItem in convertingLine.MaterialNodes)
                    {
                        var material = materialNodeItem.Key;
                        var materialNode = materialNodeItem.Value;
                        GetFinishedGoodsWarehouse(material);
                    }

                foreach (var finishedGoodsWarehouseItem in FinishedGoodsWarehouses)
                {
                    var material = finishedGoodsWarehouseItem.Key;
                    var finishedGoodsWarehouse = finishedGoodsWarehouseItem.Value;

                    finishedGoodsWarehouse.AddInternalsToGraph(graph);
                }
            }

            if (ConvertingLines.Count != 0)
            {
                var externalLocation = State.GetInternalExternalLocationCounterpart(this);
                if (externalLocation != null)
                {
                    foreach (var finishedGoodsWarehouseItem in FinishedGoodsWarehouses) {
                        var material = finishedGoodsWarehouseItem.Key;
                        var finishedGoodsWarehouse = finishedGoodsWarehouseItem.Value;
                        externalLocation.GetFinishedGoodsWarehouse(material);
                    }
                }
            }
        }

        public void AddFlowsAndDemandToGraph(Graph graph)
        {
            foreach (var outEdge in OutFlow)
            {
                var shipTo = outEdge.Key;
                foreach (var kv in outEdge.Value)
                {
                    var material = kv.Key;
                    var amount = kv.Value;

                    if (material.IsBasepaper)
                    {
                        var shipFromBasepaperWarehouse = GetBasepaperWarehouse();
                        var shipToBasepaperWarehouse = shipTo.GetBasepaperWarehouse();

                        TaggedEdge<Node, Tag> edge = null;
                        foreach (var graphOutEdge in graph.FlowGraph.OutEdges(shipFromBasepaperWarehouse.OutputNode))
                        {
                            if (graphOutEdge.Target == shipToBasepaperWarehouse.InputNode)
                            {
                                edge = graphOutEdge;
                                break;
                            }
                        }

                        if (edge == null)
                        {
                            var cost = State.TransportationCost.InternalShipmentPerKg(this, shipTo, material);
                            if (double.IsInfinity(cost) || double.IsNaN(cost))
                                continue;
                            var tag = new Tag(material) { TransportationCost = cost };
                            edge = new TaggedEdge<Node, Tag>(shipFromBasepaperWarehouse.OutputNode,
                                shipToBasepaperWarehouse.InputNode, tag);
                            graph.FlowGraph.AddEdge(edge);
                        }

                        shipFromBasepaperWarehouse.Demand += amount;
                        shipToBasepaperWarehouse.Demand -= amount;

                        edge.Tag.MetsaFlow += amount;
                    }
                    else
                    {
                        var shipFromFinishedGoodsWarehouse = GetFinishedGoodsWarehouse(material);
                        var shipToFinishedGoodsWarehouse = shipTo.GetFinishedGoodsWarehouse(material);

                        shipFromFinishedGoodsWarehouse.Demand += amount;
                        shipToFinishedGoodsWarehouse.Demand -= amount;

                        TaggedEdge<Node, Tag> edge = null;
                        foreach (var graphOutEdge in graph.FlowGraph.OutEdges(shipFromFinishedGoodsWarehouse.OutputNode))
                        {
                            if (graphOutEdge.Target == shipToFinishedGoodsWarehouse.InputNode)
                            {
                                edge = graphOutEdge;
                                break;
                            }
                        }

                        if (edge == null)
                        {
                            var cost = State.TransportationCost.InternalShipmentPerKg(this, shipTo, material);
                            if (double.IsInfinity(cost) || double.IsNaN(cost))
                                continue;
                            var tag = new Tag(material) { TransportationCost = cost };
                            edge = new TaggedEdge<Node, Tag>(shipFromFinishedGoodsWarehouse.OutputNode, shipToFinishedGoodsWarehouse.InputNode, tag);
                            graph.FlowGraph.AddEdge(edge);
                        }

                        edge.Tag.MetsaFlow += amount;
                    }
                }
            }

            foreach (var externalEdge in ToCustomerFlow)
            {
                var shipTo = externalEdge.Key;
                foreach (var kv in externalEdge.Value)
                {
                    var material = kv.Key;
                    var amount = kv.Value;

                    if (material.IsBasepaper)
                    {
                        var shipFromBasepaperWarehouse = GetBasepaperWarehouse();
                        var shipToNode = shipTo.BasepaperNode;

                        shipFromBasepaperWarehouse.Demand += amount;
                        shipToNode.Demand -= amount;

                        TaggedEdge<Node, Tag> edge = null;
                        foreach (var graphOutEdge in graph.FlowGraph.OutEdges(shipFromBasepaperWarehouse.OutputNode))
                            if (graphOutEdge.Target == shipToNode)
                            {
                                edge = graphOutEdge;
                                break;
                            }

                        if (edge == null)
                        {
                            var cost = State.TransportationCost.ExternalShipmentPerKg(this, shipTo, material);
                            if (double.IsInfinity(cost) || double.IsNaN(cost))
                                continue;
                            var tag = new Tag(material) { TransportationCost = cost };
                            edge = new TaggedEdge<Node, Tag>(shipFromBasepaperWarehouse.OutputNode, shipToNode, tag);
                            graph.FlowGraph.AddEdge(edge);
                        }

                        edge.Tag.MetsaFlow += kv.Value;

                    }
                    else
                    {
                        var shipFromFinishedGoodsWarehouse = GetFinishedGoodsWarehouse(material);
                        var shipToNode = shipTo.MaterialNodes[material];

                        shipFromFinishedGoodsWarehouse.Demand += amount;
                        shipToNode.Demand -= amount;

                        TaggedEdge<Node, Tag> edge = null;
                        foreach (var graphOutEdge in graph.FlowGraph.OutEdges(shipFromFinishedGoodsWarehouse.OutputNode))
                            if (graphOutEdge.Target == shipToNode)
                            {
                                edge = graphOutEdge;
                                break;
                            }

                        if (edge == null)
                        {
                            var cost = State.TransportationCost.ExternalShipmentPerKg(this, shipTo, material);
                            if (double.IsInfinity(cost) || double.IsNaN(cost))
                                continue;
                            var tag = new Tag(material) { TransportationCost = cost };
                            edge = new TaggedEdge<Node, Tag>(shipFromFinishedGoodsWarehouse.OutputNode, shipToNode, tag);
                            graph.FlowGraph.AddEdge(edge);
                        }

                        edge.Tag.MetsaFlow += kv.Value;
                    }
                }
            }

            if (HasBasepaperWarehouse)
            {
                var ableToProduce = new HashSet<Material>();
                foreach (var convertingLine in ConvertingLines)
                {
                    var materials = convertingLine.MaterialNodes.Keys;
                    ableToProduce.UnionWith(materials);
                }

                foreach (var finishedGoodsWarehouseItem in FinishedGoodsWarehouses)
                {
                    var material = finishedGoodsWarehouseItem.Key;
                    var finishedGoodsWarehouse = finishedGoodsWarehouseItem.Value;

                    if (finishedGoodsWarehouse.Demand > 0 && ableToProduce.Contains(material))
                    {
                        var demand = finishedGoodsWarehouse.Demand;
                        finishedGoodsWarehouse.Demand -= demand;
                        GetBasepaperWarehouse().Demand += demand;
                    }
                }

                GetBasepaperWarehouse().AddEdgesToGraph(graph);
            }

            if (HasFinishedGoodsWarehouse)
            {
                foreach (var convertingLine in ConvertingLines)
                {
                    convertingLine.AddEdgesToGraph(graph);

                    {
                        var tag = new Tag(State.GetBasepaperMixMaterial()) { TransportationCost = 0 };
                        var edge = new TaggedEdge<Node, Tag>(GetBasepaperWarehouse().OutputNode,
                            convertingLine.InputNode,
                            tag);
                        graph.FlowGraph.AddEdge(edge);
                    }

                    foreach (var materialNodeItem in convertingLine.MaterialNodes)
                    {
                        var material = materialNodeItem.Key;
                        var materialNode = materialNodeItem.Value;

                        var tag = new Tag(material) {TransportationCost = 0};
                        var edge = new TaggedEdge<Node, Tag>(materialNode, GetFinishedGoodsWarehouse(material).InputNode,
                            tag);
                        graph.FlowGraph.AddEdge(edge);
                    }
                }

                foreach (var finishedGoodsWarehouse in FinishedGoodsWarehouses.Values)
                    finishedGoodsWarehouse.AddEdgesToGraph(graph);
            }

            if (ConvertingLines.Count != 0)
            {
                var externalLocation = State.GetInternalExternalLocationCounterpart(this);

                if (externalLocation != null)
                {
                    foreach (var finishedGoodsWarehouseItem in FinishedGoodsWarehouses)
                    {
                        var material = finishedGoodsWarehouseItem.Key;
                        var finishedGoodsWarehouse = finishedGoodsWarehouseItem.Value;

                        var externalWarehouse = externalLocation.GetFinishedGoodsWarehouse(material);

                        var cost = State.TransportationCost.InternalShipmentPerKg(this, externalLocation, material);

                        if (double.IsNaN(cost))
                        {
                            if (PlantCode.Substring(0, 2) == "79") cost = 10 / 1000;
                            if (PlantCode.Substring(0, 2) == "43") cost = 5 / 1000;
                        }
                        
                        if (double.IsNaN(cost) || double.IsInfinity(cost))
                            continue;

                        var tag = new Tag(material) {TransportationCost = cost};
                        var edge = new TaggedEdge<Node, Tag>(finishedGoodsWarehouse.ExternalWarehouseNode,
                            externalWarehouse.InputNode, tag);
                        graph.FlowGraph.AddEdge(edge);
                    }
                }
            }
        }

        public void InternalClearing()
        {
            if (HasBasepaperWarehouse)
                GetBasepaperWarehouse().InternalClearing();

            if (HasFinishedGoodsWarehouse)
                foreach (var finishedGoodsWarehouse in FinishedGoodsWarehouses.Values)
                    finishedGoodsWarehouse.InternalClearing();
        }

        public void LoadFlowFromGraph(Graph graph) {
            if (HasBasepaperWarehouse) {
                var basepaperMaterial = State.GetBasepaperMixMaterial();
                var outNode = GetBasepaperWarehouse ().OutputNode;
                foreach (var outEdge in graph.FlowGraph.OutEdges(outNode)) {
                    var target = outEdge.Target;
                    var amount = outEdge.Tag.SolvedFlow;
                    var targetBelongsTo = target.BelongsTo;

                    var ts = new TypeSwitch ()
                        .Case ((BasepaperWarehouse shipToLocation) => AddSolvedFlowTo (basepaperMaterial, shipToLocation.Location, amount))
                        .Case ((Customer shipToCustomer) => AddSolvedFlowToCustomer (basepaperMaterial, shipToCustomer, amount));

                    if (targetBelongsTo != null)
                        ts.Switch (targetBelongsTo);
                }
            }

            if (HasFinishedGoodsWarehouse)
            {
                foreach (var finishedGoodsWarehouseItem in FinishedGoodsWarehouses)
                {
                    var material = finishedGoodsWarehouseItem.Key;
                    var finishedGoodsWarehouse = finishedGoodsWarehouseItem.Value;

                    foreach (var outEdge in graph.FlowGraph.OutEdges(finishedGoodsWarehouse.OutputNode))
                    {
                        var target = outEdge.Target;
                        var amount = outEdge.Tag.SolvedFlow;
                        var targetBelongsTo = target.BelongsTo;

                        var ts = new TypeSwitch()
                            .Case((FinishedGoodsWarehouse shipToLocation) => AddSolvedFlowTo(material, shipToLocation.Location, amount))
                            .Case((Customer shipToCustomer) => AddSolvedFlowToCustomer(material, shipToCustomer, amount));

                        if (targetBelongsTo != null)
                            ts.Switch(targetBelongsTo);
                    }
                }
            }
        }

        public void AddFlowTo(Material material, Location to, long amount) {
            InternalAddFlowTo (false, material, to, amount);
        }

        public void AddSolvedFlowTo(Material material, Location to, long amount) {
            InternalAddFlowTo (true, material, to, amount);
        }

        private void InternalAddFlowTo(bool solved, Material material, Location to, long amount)
        {
            if (material.IsBasepaper)
            {
                if (!HasBasepaperWarehouse)
                    throw new InvalidDataException(string.Format("Ship from location {0} has no basepaper warehouse.", PlantCode));
                if (!to.HasBasepaperWarehouse)
                    throw new InvalidDataException(string.Format("Ship to location {0} has no basepaper warehouse.", PlantCode));
            }

            if (material.IsFinishedGood && !HasFinishedGoodsWarehouse)
            {
                if (!HasFinishedGoodsWarehouse)
                    throw new InvalidDataException(string.Format("Ship from location {0} has no finished good warehouse.", PlantCode));
                if (!to.HasFinishedGoodsWarehouse)
                    throw new InvalidDataException(string.Format("Ship to location {0} has no finished good warehouse.", PlantCode));
            }

            var outFlow = solved ? SolvedOutFlow : OutFlow;
            var inFlow = solved ? to.SolvedInFlow : to.InFlow;

            if (!outFlow.ContainsKey(to))
                outFlow.Add(to, new Dictionary<Material, long>());

            if (!outFlow[to].ContainsKey(material))
                outFlow[to].Add(material, 0);

            outFlow[to][material] += amount;

            if (!inFlow.ContainsKey(this))
                inFlow.Add(this, new Dictionary<Material, long>());

            if (!inFlow[this].ContainsKey(material))
                inFlow[this].Add(material, 0);

            inFlow[this][material] += amount;
        }

        public void AddFlowToCustomer(Material material, Customer customer, long amount) {
            InternalAddFlowToCustomer (false, material, customer, amount);
        }

        public void AddSolvedFlowToCustomer(Material material, Customer customer, long amount) {
            InternalAddFlowToCustomer (true, material, customer, amount);
        }

        public void InternalAddFlowToCustomer(bool solved, Material material, Customer customer, long amount)
        {
            if (material.IsBasepaper && !HasBasepaperWarehouse)
                throw new InvalidDataException(string.Format("Adding basepaper flow to customer from plant {0} but no basepaper warehouse.", PlantCode));

            if (material.IsFinishedGood && !HasFinishedGoodsWarehouse)
                throw new InvalidDataException(string.Format("Adding finished good flow to customer from plant {0} but no finished good warehouse.", PlantCode));

            var toCustomerFlow = solved ? SolvedToCustomerFlow : ToCustomerFlow;
            var customerInFlow = solved ? customer.SolvedInFlow : customer.InFlow;

            if (!toCustomerFlow.ContainsKey(customer))
                toCustomerFlow.Add(customer, new Dictionary<Material, long>());

            if (!toCustomerFlow[customer].ContainsKey(material))
                toCustomerFlow[customer].Add(material, 0);

            toCustomerFlow[customer][material] += amount;

            if (!customerInFlow.ContainsKey(this))
                customerInFlow.Add(this, new Dictionary<Material, long>());

            if (!customerInFlow[this].ContainsKey(material))
                customerInFlow[this].Add(material, 0);

            customerInFlow[this][material] += amount;
        }
    }
}
