﻿using System;

namespace Metsa
{
    public class Tag
    {
        public long MetsaFlow;
        public long SolvedFlow;

        public Material Material;

        public Tag(Material material)
        {
            Material = material;
        }

        public static long InfiniteFlow = 0x7fffffff;

        public long LowerBound = 0;
        public long UpperBound = InfiniteFlow;

        private double _transportationCostInternal;
        public static double SolverCostScaleFactor = 1000000;

        public long SolverCost
        {
            get { return (long)Math.Round(_transportationCostInternal * SolverCostScaleFactor); }
            set { _transportationCostInternal = value / SolverCostScaleFactor; }
        }

        public double TransportationCost
        {
            get { return _transportationCostInternal; }
            set { _transportationCostInternal = value; }
        }
    }
}
