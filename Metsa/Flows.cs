﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Metsa
{
    public class Flows
    {
        public MetsaState State;
        public List<string> Headers = new List<string>();
        public List<FlowsRow> Rows = new List<FlowsRow>();
        public Dictionary<FlowsRow, bool> RowsAssigned = new Dictionary<FlowsRow, bool>();

        public int LoadFromFile(string fileName)
        {
            int lineNumber = 1; /* one for header */
            using (var inStream = new StreamReader(fileName))
            {
                var headersLine = inStream.ReadLine();
                Headers.AddRange(headersLine.Split(new[] { ';' }));
                if (Headers.Count != 15)
                    throw new InvalidDataException("Expected 15 columns in flows file");

                while (!inStream.EndOfStream)
                {
                    ++lineNumber;
                    var line = inStream.ReadLine();

                    FlowsRow row;
                    if (!FlowsRow.TryParse(State, line, out row))
                        continue;

                    row.SourceLocator = new SourceLocator { FileName = fileName, LineNumber = lineNumber, Line = line };
                    Rows.Add(row);
                    RowsAssigned.Add(row, false);
                }
            }

            Console.WriteLine("Parser {0} out of {1} lines (flow)", Rows.Count, lineNumber);

            return Rows.Count;
        }
    }
}
