﻿using System;

namespace Metsa
{
    public class SourceLocator
    {
        public string Line;
        public string FileName;
        public int LineNumber;
    }
}
