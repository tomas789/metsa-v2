﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metsa
{
    public class Haversine
    {
        private const double EARTH_RADIUS_KM = 6371;

        public static double GetDistanceKM(double latX, double lonX, double latY, double lonY)
        {
            Func<double, double> toRad = x => x * (Math.PI / 180);

            double dLat = toRad(latY - latX);
            double dLon = toRad(lonY - latY);

            double a = Math.Pow(Math.Sin(dLat / 2), 2) +
                Math.Cos(toRad(latX)) * Math.Cos(toRad(latY)) *
                Math.Pow(Math.Sin(dLon / 2), 2);

            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            double distance = EARTH_RADIUS_KM * c;
            return distance;
        }
    }
}
