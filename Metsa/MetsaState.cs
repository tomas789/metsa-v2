﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using QuickGraph;

namespace Metsa
{
    public class MetsaState
    {
        public Flows Flows;
        public Materials Materials;
        public ConvertingLoader ConvertingLoader;
        public Geolocation Geolocation;
        public TransportationCost TransportationCost;
        public FinishedGoodsSplitter FinishedGoodsSplitter;

        public Material BasepaperMaterial = null;
        public Dictionary<string, Material> MaterialsContainer = new Dictionary<string, Material>();
        public Dictionary<string, Customer> CustomersByShipTo = new Dictionary<string, Customer>();
        public List<Location> Locations = new List<Location>();

        public List<Node> Nodes = new List<Node>();

        public Node SupplyNode;

        public Material BasepaperMixMaterial;

        public Material GetBasepaperMixMaterial()
        {
            if (BasepaperMixMaterial == null)
                BasepaperMixMaterial = new Material() {IsBasepaper = true, MaterialName = "Basepaper Mix"};

            return BasepaperMixMaterial;
        }

        public Material GetMaterial(string materialName)
        {
            if (!MaterialsContainer.ContainsKey(materialName))
                MaterialsContainer.Add(materialName, new Material { MaterialName = materialName });
            return MaterialsContainer[materialName];
        }

        public void InternalClearing()
        {
            foreach (var loc in Locations)
                loc.InternalClearing();
        }

        public Customer GetCustomer(string shipTo)
        {
            if (!CustomersByShipTo.ContainsKey(shipTo))
                CustomersByShipTo.Add(shipTo, new Customer { State = this });
            return CustomersByShipTo[shipTo];
        }

        public Graph GenerateGraph()
        {
            var graph = new Graph(this);

            var externalLocations = new List<Location>();
            foreach (var loc in Locations)
            {
                if (loc.ConvertingLines != null && loc.ConvertingLines.Count != 0)
                    loc.AddInternalsToGraph(graph);
                else 
                    externalLocations.Add(loc);
            }

            foreach (var loc in externalLocations)
                loc.AddInternalsToGraph(graph);

            foreach (var customer in CustomersByShipTo.Values)
                customer.AddInternalsToGraph(graph);

            AddZeroInternalFlow();
            AddZeroExternalFlow();

            foreach (var loc in Locations)
                loc.AddFlowsAndDemandToGraph(graph);

            SupplyNode = new Node(this);
            graph.FlowGraph.AddVertex(SupplyNode);

            /* TODO: Why this makes solution infeasible? */
            //foreach (var loc in Locations)
            //{
            //    if (loc.HasBasepaperWarehouse)
            //    {
            //        var tag = new Tag { TransportationCost = 0 };
            //        var edge = new TaggedEdge<Node, Tag>(SupplyNode, loc.GetBasepaperWarehouse().InputNode, tag);
            //        graph.FlowGraph.AddEdge(edge);

            //        if (loc.GetBasepaperWarehouse().Demand < 0)
            //        {
            //            var demand = loc.GetBasepaperWarehouse().Demand;
            //            loc.GetBasepaperWarehouse().Demand -= demand;
            //            SupplyNode.Demand += demand;
            //        }
            //    }
            //} 

            InternalClearing();

            return graph;
        }

        public void LoadFlowFromGraph(Graph graph)
        {
            foreach (var loc in Locations)
                loc.LoadFlowFromGraph(graph);
        }

        private Dictionary<Location, Location> _internalExternalLocationConterpart = new Dictionary<Location, Location>();

        public Location GetInternalExternalLocationCounterpart(Location location)
        {
            if (!_internalExternalLocationConterpart.ContainsKey(location))
            {
                Location counterpart = null;
                var candidates = (from loc in Locations
                                  where loc.PlantCode == location.PlantCode && loc.PostalCode != location.PostalCode
                                  select loc).ToArray();

                if (candidates.Length == 1)
                    counterpart = candidates[0];

                _internalExternalLocationConterpart.Add(location, counterpart);
            }

            return _internalExternalLocationConterpart[location];
        }

        public void MatchFlows()
        {
            var locationsByStorageId = new Dictionary<string, Location>();
            foreach (var loc in Locations)
            {
                if (loc.HasFinishedGoodsWarehouse)
                    foreach (var finishedGoodWarehouseName in loc.FinishedGoodsWarehouseNames)
                        locationsByStorageId.Add(loc.PlantCode + "/" + finishedGoodWarehouseName, loc);

                if (loc.HasBasepaperWarehouse)
                    foreach (var basepaperWarehouseName in loc.BasepaperWarehouseNames)
                        locationsByStorageId.Add(loc.PlantCode + "/" + basepaperWarehouseName, loc);
            }

            foreach (var row in Flows.Rows)
            {
                Location shipFrom;
                var storageId = row.StorageId.Length == 9 ? row.StorageId : "4310/" + row.StorageId;
                if (!locationsByStorageId.TryGetValue(storageId, out shipFrom))
                {
                    row.MatchingMessage = "No Ship-From location found";
                    continue;
                }

                row.ShipFromLoaded = shipFrom;

                if (row.ShipTo.Length == 4 && row.ShipTo != "4510" && row.ShipTo != "5110" && row.ShipTo != "5120" && row.ShipTo != "9640" && row.ShipTo != "7080")
                {
                    /* Ship-To warehouse */
                    var shipTo = (from loc in Locations
                                  where loc.PlantCode == row.ShipTo && row.PostalCode == loc.PostalCode
                                  select loc).ToArray();

                    if (shipTo.Length == 1)
                    {
                        if (shipFrom == shipTo[0])
                            shipTo[0] = GetInternalExternalLocationCounterpart(shipTo[0]);

                        shipFrom.AddFlowTo(row.Material, shipTo[0], row.Kg);

                        row.ShipToLocationLoaded = shipTo[0];
                        row.MatchingMessage = "Success";
                    }
                    else
                    {
                        row.MatchingMessage = string.Format("Ship-To Location count: {0}", shipTo.Length);
                    }
                }
                else
                {
                    /* Ship-To customer */
                    var shipToCustomer = GetCustomer(row.ShipTo);
                    shipFrom.AddFlowToCustomer(row.Material, shipToCustomer, row.Kg);

                    row.ShipToCustomerLoaded = shipToCustomer;
                    row.MatchingMessage = "Success (customer)";

                    if (string.IsNullOrEmpty(shipToCustomer.ShipTo))
                    {
                        shipToCustomer.ShipTo = row.ShipTo;
                        shipToCustomer.Country = row.Country;
                        shipToCustomer.CityOfShipToPart = row.CityOfShipToPart;
                        shipToCustomer.PostalCode = row.PostalCode;
                        shipToCustomer.Region = row.Country;

                        Geolocation.TryGeolocate(row.Country, row.PostalCode, out shipToCustomer.Latitude, out shipToCustomer.Longitude);
                    }
                }
            }

            var matchingLog = @"FlowMatching.csv";
            using (var outStream = new StreamWriter(matchingLog))
            {
                outStream.WriteLine(string.Join(";", Flows.Headers.Concat(new[] { "Matching result" })));
                foreach (var row in Flows.Rows)
                {
                    outStream.WriteLine(row.SourceLocator.Line + ";" + row.MatchingMessage);
                }
            }

            Console.WriteLine("Parovaci log zapsany v " + matchingLog);
        }

        public void AddZeroInternalFlow()
        {
            foreach (var shipFrom in Locations)
            {
                var shipFromMaterials = new HashSet<Material>(shipFrom.FinishedGoodsWarehouses.Keys);
                foreach (var shipTo in Locations)
                {
                    var commonMaterials = shipFromMaterials.Intersect(shipTo.FinishedGoodsWarehouses.Keys);

                    foreach (var material in commonMaterials)
                        shipFrom.AddFlowTo(material, shipTo, 0);

                    if (shipFrom.HasBasepaperWarehouse && shipTo.HasBasepaperWarehouse)
                        shipFrom.AddFlowTo(GetBasepaperMixMaterial(), shipTo, 0);
                }
            }
        }

        public void AddZeroExternalFlow()
        {
            foreach (var shipFrom in Locations)
            {
                var shipFromMaterials = new HashSet<Material>(shipFrom.FinishedGoodsWarehouses.Keys);
                foreach (var shipTo in CustomersByShipTo.Values)
                {
                    var commonMaterials = shipFromMaterials.Intersect(shipTo.MaterialNodes.Keys);

                    foreach (var material in commonMaterials)
                        shipFrom.AddFlowToCustomer(material, shipTo, 0);

                    if (shipFrom.HasBasepaperWarehouse)
                        shipFrom.AddFlowToCustomer(GetBasepaperMixMaterial(), shipTo, 0);
                }
            }
        }
    }
}
