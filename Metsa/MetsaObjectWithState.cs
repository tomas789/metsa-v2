﻿using System;

namespace Metsa
{
    public class MetsaObjectWithState
    {
        public MetsaState State;
        public MetsaObjectWithState(MetsaState state)
        {
            State = state;
        }
    }
}
