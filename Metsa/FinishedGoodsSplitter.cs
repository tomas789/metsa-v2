﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickGraph;

namespace Metsa
{
    public class FinishedGoodsSplitter
    {
        public MetsaState State;
        public bool CapacitiesChanged = false;

        public void setCapacities(Graph graph)
        {
            foreach (var location in State.Locations)
            {
                if (!location.HasFinishedGoodsWarehouse) continue;
                if (location.ConvertingLines.Count == 0) continue;
                if (location.FinishedGoodsWarehousesOverallCapacity == Tag.InfiniteFlow) continue;
                if (State.GetInternalExternalLocationCounterpart(location) == null) continue;

                var externalLocation = State.GetInternalExternalLocationCounterpart(location);

                long usedCapacity = 0;
                foreach (var kv in location.FinishedGoodsWarehouses)
                {
                    usedCapacity += kv.Value.CapacityEdge.Tag.SolvedFlow;
                }

                foreach (var kv in location.FinishedGoodsWarehouses)
                {
                    /*
                    TaggedEdge<Node, Tag> externalWarehouseInputEdgee = null;
                    foreach (var edge in graph.FlowGraph.OutEdges(kv.Value.ExternalWarehouseNode))
                        if (edge.Target == externalLocation.GetFinishedGoodsWarehouse(kv.Key).InputNode)
                            externalWarehouseInputEdgee = edge;

                    if (externalWarehouseInputEdgee == null)
                        throw new Exception();

                    var shipableCustomers = new HashSet<Node>();
                    foreach (var edge in graph.FlowGraph.OutEdges(kv.Value.OutputNode))
                        shipableCustomers.Add(edge.Target);

                    foreach (var edge in graph.FlowGraph.OutEdges(externalLocation.GetFinishedGoodsWarehouse(kv.Key).OutputNode))
                        shipableCustomers.Remove(edge.Target);

                    if (shipableCustomers.Count != 0) 
                        throw new Exception();

                    foreach (var finishedGoodsWarehouseItem in externalLocation.FinishedGoodsWarehouses)
                    {
                        var material = finishedGoodsWarehouseItem.Key;
                        var finishedGoodsWarehouse = finishedGoodsWarehouseItem.Value;

                        if (finishedGoodsWarehouse.CapacityEdge == null)
                            throw new Exception();

                        if (finishedGoodsWarehouse.CapacityEdge.Tag.UpperBound != Tag.InfiniteFlow)
                            throw new Exception();
                    }
                     */

                    kv.Value.CapacityEdge.Tag.UpperBound = (kv.Value.CapacityEdge.Tag.SolvedFlow / usedCapacity) * location.FinishedGoodsWarehousesOverallCapacity;
                    if (kv.Value.CapacityEdge.Tag.UpperBound < 0)
                    {
                        Console.WriteLine("zero capacity");
                    }
                }

            }
        }
    }
}
