﻿using System;
using System.Collections.Generic;

namespace Metsa
{
    public class Customer
    {
        public string ShipTo;
        public string CustomerName;
        public string Country;
        public string PostalCode;
        public string CityOfShipToPart;
        public string Region;

        public MetsaState State;

        public double Latitude;
        public double Longitude;

        public Node BasepaperNode;
        public Dictionary<Material, Node> MaterialNodes = new Dictionary<Material, Node>();

        public void AddInternalsToGraph(Graph graph)
        {
            BasepaperNode = new Node(State) {BelongsTo = this};
            foreach (var inEdge in InFlow)
                foreach (var material in inEdge.Value.Keys)
                {
                    if (!MaterialNodes.ContainsKey(material))
                    {
                        var node = new Node(State) { BelongsTo = this };
                        MaterialNodes.Add(material, node);
                    }
                }

            graph.FlowGraph.AddVertex(BasepaperNode);
            graph.FlowGraph.AddVertexRange(MaterialNodes.Values);
        }

        public Dictionary<Location, Dictionary<Material, long>> InFlow = new Dictionary<Location, Dictionary<Material, long>>();
        public Dictionary<Location, Dictionary<Material, long>> SolvedInFlow = new Dictionary<Location, Dictionary<Material, long>>();
    }
}
