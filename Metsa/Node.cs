﻿using System;

namespace Metsa
{
    public class Node : MetsaObjectWithState
    {
        public int NodeNumber;
        public long Demand;
        public long SolvedDemand;
        public int Id;

        public object BelongsTo = null;

        public Location Location;
        public BasepaperWarehouse BasepaperWarehouse;
        public FinishedGoodsWarehouse FinishedGoodsWarehouse;
        public ConvertingLine ConvertingLine;

        public Node(MetsaState state)
            : base(state)
        {
            Id = state.Nodes.Count;
            state.Nodes.Add(this);
        }

        public Location tryGetLocation()
        {
            if (Location != null) return Location;
            if (BelongsTo is BasepaperWarehouse) return ((BasepaperWarehouse) BelongsTo).Location;
            if (BelongsTo is FinishedGoodsWarehouse) return ((FinishedGoodsWarehouse) BelongsTo).Location;
            if (BelongsTo is ConvertingLine) return ((ConvertingLine)BelongsTo).Location;
            return null;
        }
    }
}
