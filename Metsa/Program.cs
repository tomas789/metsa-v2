﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Metsa
{
    class MainClass
    {
        public static string GetPathByParts(params object[] parts) {
            return string.Join (Path.DirectorySeparatorChar.ToString (), parts.Select (p => p.ToString ()));
        }

        public static MetsaState GetTheoreticalModel()
        {
            var metsaState = new MetsaState();

            var materials = new Materials { State = metsaState };
            materials.LoadFromFile(GetPathByParts("CSV", "Materialy.csv"));
            metsaState.Materials = materials;

            var flows = new Flows { State = metsaState };
            flows.LoadFromFile(GetPathByParts("CSV", "Toky.csv"));
            metsaState.Flows = flows;

            var convertings = new ConvertingLoader { State = metsaState };
            convertings.LoadFromFile(GetPathByParts("CSV", "Konverting.csv"));
            metsaState.ConvertingLoader = convertings;

            var geolocation = new Geolocation { State = metsaState };
            geolocation.LoadFromFile(GetPathByParts("CSV", "Geolokace.csv"));
            metsaState.Geolocation = geolocation;

            metsaState.Locations.AddRange(new[] {
                new Location(metsaState) { /* Raubach internal */
                    PlantCode = "9620", PlantName = "Raubach", PostalCode = "56316",
                    Latitude = 50.574889, Longitude = 7.625152,
                    HasPaperMill = true,
                    BasepaperWarehouseNames = new List<string> { "1000" },
                    FinishedGoodsWarehouseNames = new List<string> { "2000" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                    ConvertingLines = new[] {
                        "IR 5", "MF 4", "Sincro", "T 1", "T 3", "WD 3", "WD 4", "WD 5", "WD 6",
                        "R 1", "R 10", "R 3", "R 4", "R 5", "R 6", "R 7", "R 8", "R 9",
                        "L 14", "L 15", "L 16", "L 2", "L 3", "L 6", "L 14", "L 15", "L 16", "L2", "L3", "L6",
                        "COMBO", "L 17", "SR 5", "SR 7", "SR 9", "SR5", "SR7", "SR8", "ZZ", "ZZ 4", "ZZ 5", "ZZ4", "ZZ5"
                    }.Select(name => new ConvertingLine(metsaState, name)).ToList()
                },
                new Location(metsaState) { /* Raubach external */
                    PlantCode = "9620", PlantName = "Raubach", PostalCode = "56170",
                    Latitude = 50.429876, Longitude = 7.562686,
                    HasPaperMill = false,
                    FinishedGoodsWarehouseNames = new List<string> { "2001" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                },
                new Location(metsaState) { /* Kreuzau internal */
                    PlantCode = "9610", PlantName = "Kreuzau", PostalCode = "52372",
                    Latitude = 50.7472121, Longitude = 6.4906405,
                    HasPaperMill = true,
                    BasepaperWarehouseNames = new List<string> { "1000" },
                    FinishedGoodsWarehouseNames = new List<string> { "2000" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                    ConvertingLines = new[] {
                        "IR 5", "MF 4", "Sincro", "T 1", "T 3", "WD 3", "WD 4", "WD 5", "WD 6",
                        "R 1", "R 10", "R 3", "R 4", "R 5", "R 6", "R 7", "R 8", "R 9",
                        "L 14", "L 15", "L 16", "L 2", "L 3", "L 6", "L 14", "L 15", "L 16", "L2", "L3", "L6",
                        "COMBO", "L 17", "SR 5", "SR 7", "SR 9", "SR5", "SR7", "SR8", "ZZ", "ZZ 4", "ZZ 5", "ZZ4", "ZZ5"
                    }.Select(name => new ConvertingLine(metsaState, name)).ToList()
                },
                new Location(metsaState) { /* Kreuzau external */
                    PlantCode = "9610", PlantName = "Kreuzau", PostalCode = "52353",
                    Latitude = 50.787254, Longitude = 6.488254,
                    HasPaperMill = false,
                    BasepaperWarehouseNames = new List<string> { "1001" },
                    FinishedGoodsWarehouseNames = new List<string> { "2500", /* internal */ "2001" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                },
                new Location(metsaState) { /* Stotzheim internal */
                    PlantCode = "9630", PlantName = "Stotzheim", PostalCode = "53881",
                    Latitude = 50.6574392, Longitude = 6.7899945,
                    HasPaperMill = true,
                    BasepaperWarehouseNames = new List<string> { "1000" },
                    FinishedGoodsWarehouseNames = new List<string> { "2000" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                    ConvertingLines = new[] {
                        "IR 5", "MF 4", "Sincro", "T 1", "T 3", "WD 3", "WD 4", "WD 5", "WD 6",
                        "R 1", "R 10", "R 3", "R 4", "R 5", "R 6", "R 7", "R 8", "R 9",
                        "L 14", "L 15", "L 16", "L 2", "L 3", "L 6", "L 14", "L 15", "L 16", "L2", "L3", "L6",
                        "COMBO", "L 17", "SR 5", "SR 7", "SR 9", "SR5", "SR7", "SR8", "ZZ", "ZZ 4", "ZZ 5", "ZZ4", "ZZ5"
                    }.Select(name => new ConvertingLine(metsaState, name)).ToList()
                },
                new Location(metsaState) { /* Stotzheim external */
                    PlantCode = "9630", PlantName = "Stotzheim", PostalCode = "53879",
                    Latitude = 50.6574392, Longitude = 6.7899945,
                    HasPaperMill = false,
                    FinishedGoodsWarehouseNames = new List<string> { "2001", /* TODO: Validation needed */ "2002" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                },
                new Location(metsaState) { /* Zilina internal */
                    PlantCode = "7910", PlantName = "Zilina", PostalCode = "011 61",
                    Latitude = 49.1956597, Longitude = 18.538215,
                    HasPaperMill = true,
                    BasepaperWarehouseNames = new List<string> { "1000", "1013", "1027" },
                    FinishedGoodsWarehouseNames = new List<string> { "2500", /* internal */ "2000" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                    ConvertingLines = new[] {
                        "IR 5", "MF 4", "Sincro", "T 1", "T 3", "WD 3", "WD 4", "WD 5", "WD 6",
                        "R 1", "R 10", "R 3", "R 4", "R 5", "R 6", "R 7", "R 8", "R 9",
                        "L 14", "L 15", "L 16", "L 2", "L 3", "L 6", "L 14", "L 15", "L 16", "L2", "L3", "L6",
                        "COMBO", "L 17", "SR 5", "SR 7", "SR 9", "SR5", "SR7", "SR8", "ZZ", "ZZ 4", "ZZ 5", "ZZ4", "ZZ5"
                    }.Select(name => new ConvertingLine(metsaState, name)).ToList()
                },
                new Location(metsaState) { /* Zilina external */
                    PlantCode = "7910", PlantName = "Zilina", PostalCode = "010 04",
                    Latitude = 49.194279, Longitude = 18.720064,
                    HasPaperMill = false,
                    FinishedGoodsWarehouseNames = new List<string> { "2100" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                },
                new Location(metsaState) { /* Krapkowice internal */
                    PlantCode = "4310", PlantName = "Krapkowice", PostalCode = "47-300",
                    Latitude = 50.4745052, Longitude = 17.9651385,
                    HasPaperMill = true,
                    BasepaperWarehouseNames = new List<string> {
                        "1010",
                        "1020",
                        "1110",
                        "1111",
                        "1140",
                        "1141",
                        "1210",
                        "1211", /* internal */
                        "1000"
                    },
                    FinishedGoodsWarehouseNames = new List<string> { "2200", "2500", /* internal */ "2000" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                    ConvertingLines = new[] {
                        "IR 5", "MF 4", "Sincro", "T 1", "T 3", "WD 3", "WD 4", "WD 5", "WD 6",
                        "R 1", "R 10", "R 3", "R 4", "R 5", "R 6", "R 7", "R 8", "R 9",
                        "L 14", "L 15", "L 16", "L 2", "L 3", "L 6", "L 14", "L 15", "L 16", "L2", "L3", "L6",
                        "COMBO", "L 17", "SR 5", "SR 7", "SR 9", "SR5", "SR7", "SR8", "ZZ", "ZZ 4", "ZZ 5", "ZZ4", "ZZ5"
                    }.Select(name => new ConvertingLine(metsaState, name)).ToList()
                },
                new Location(metsaState) { /* Krapkowice external */
                    PlantCode = "4310", PlantName = "Krapkowice", PostalCode = "47-303",
                    Latitude = 50.478698, Longitude = 17.994525,
                    HasPaperMill = false,
                    FinishedGoodsWarehouseNames = new List<string> { "2101", /* internal */ "2001" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                }
            });

            var transportationCost = new TransportationCost { State = metsaState };
            metsaState.TransportationCost = transportationCost;

            metsaState.AddZeroInternalFlow();
            metsaState.AddZeroExternalFlow();

            metsaState.MatchFlows();

            metsaState.FinishedGoodsSplitter = new FinishedGoodsSplitter { State = metsaState };

            return metsaState;
        }

        public static MetsaState GetCurrentSetupModel()
        {
            var metsaState = new MetsaState();

            var materials = new Materials { State = metsaState };
            materials.LoadFromFile(@"CSV\Materialy.csv");
            metsaState.Materials = materials;

            var flows = new Flows { State = metsaState };
            flows.LoadFromFile(@"CSV\Toky.csv");
            metsaState.Flows = flows;

            var convertings = new ConvertingLoader { State = metsaState };
            convertings.LoadFromFile(@"CSV\Konverting.csv");
            metsaState.ConvertingLoader = convertings;

            var geolocation = new Geolocation { State = metsaState };
            geolocation.LoadFromFile(@"CSV\Geolokace.csv");
            metsaState.Geolocation = geolocation;

            metsaState.Locations.AddRange(new[] {
                new Location(metsaState) { /* Raubach internal */
                    PlantCode = "9620", PlantName = "Raubach", PostalCode = "56316",
                    Latitude = 50.574889, Longitude = 7.625152,
                    HasPaperMill = true,
                    BasepaperWarehouseNames = new List<string> { "1000" },
                    BasepaperWarehousesOverallCapacity = 108700 * 1000,
                    FinishedGoodsWarehouseNames = new List<string> { "2000" },
                    FinishedGoodsWarehousesOverallCapacity = 102830 * 1000,
                    //FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                    ConvertingLines = new[] {
                        "IR 5",
                        "MF 4",
                        "Sincro",
                        "T 1",
                        "T 3",
                        "WD 3",
                        "WD 4",
                        "WD 5",
                        "WD 6"
                    }.Select(name => new ConvertingLine(metsaState, name)).ToList()
                },
                new Location(metsaState) { /* Raubach external */
                    PlantCode = "9620", PlantName = "Raubach", PostalCode = "56170",
                    Latitude = 50.429876, Longitude = 7.562686,
                    HasPaperMill = false,
                    FinishedGoodsWarehouseNames = new List<string> { "2001" },
                    //FinishedGoodsWarehousesOverallCapacity = 27952 * 1000,
                    //FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                },
                new Location(metsaState) { /* Kreuzau internal */
                    PlantCode = "9610", PlantName = "Kreuzau", PostalCode = "52372",
                    Latitude = 50.7472121, Longitude = 6.4906405,
                    HasPaperMill = true,
                    BasepaperWarehouseNames = new List<string> { "1000" },
                    BasepaperWarehousesOverallCapacity = 189748 * 1000,
                    FinishedGoodsWarehouseNames = new List<string> { "2000" },
                    FinishedGoodsWarehousesOverallCapacity = 151820 * 1000,
                    //FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                    ConvertingLines = new[] { "R 1", "R 10", "R 3", "R 4", "R 5", "R 6", "R 7", "R 8", "R 9" }.Select(name => new ConvertingLine(metsaState, name)).ToList()
                },
                new Location(metsaState) { /* Kreuzau external */
                    PlantCode = "9610", PlantName = "Kreuzau", PostalCode = "52353",
                    Latitude = 50.787254, Longitude = 6.488254,
                    HasPaperMill = false,
                    BasepaperWarehouseNames = new List<string> { "1001" },
                    BasepaperWarehousesOverallCapacity = 38378 * 1000,
                    FinishedGoodsWarehouseNames = new List<string> { "2500", /* internal */ "2001" },
                    //FinishedGoodsWarehousesOverallCapacity = (252 + 132472) * 1000,
                    //FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                },
                new Location(metsaState) { /* Stotzheim internal */
                    PlantCode = "9630", PlantName = "Stotzheim", PostalCode = "53881",
                    Latitude = 50.6574392, Longitude = 6.7899945,
                    HasPaperMill = true,
                    BasepaperWarehouseNames = new List<string> { "1000" },
                    BasepaperWarehousesOverallCapacity = 30948 * 1000,
                    FinishedGoodsWarehouseNames = new List<string> { "2000" },
                    FinishedGoodsWarehousesOverallCapacity = 28169 * 1000,
                    //FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                },
                new Location(metsaState) { /* Stotzheim external */
                    PlantCode = "9630", PlantName = "Stotzheim", PostalCode = "53879",
                    Latitude = 50.6574392, Longitude = 6.7899945,
                    HasPaperMill = false,
                    FinishedGoodsWarehouseNames = new List<string> { "2001", /* TODO: Validation needed */ "2002" },
                    //FinishedGoodsWarehousesOverallCapacity = 48547 * 1000,
                    //FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                },
                new Location(metsaState) { /* Zilina internal */
                    PlantCode = "7910", PlantName = "Zilina", PostalCode = "011 61",
                    Latitude = 49.1956597, Longitude = 18.538215,
                    HasPaperMill = true,
                    BasepaperWarehouseNames = new List<string> { "1000", "1013", "1027" },
                    FinishedGoodsWarehouseNames = new List<string> { "2500", /* internal */ "2000" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                    ConvertingLines = new[] {
                        "L 14",
                        "L 15",
                        "L 16",
                        "L 2",
                        "L 3",
                        "L 6",
                        "L 14",
                        "L 15",
                        "L 16",
                        "L2",
                        "L3",
                        "L6"
                    }.Select(name => new ConvertingLine(metsaState, name)).ToList()
                },
                new Location(metsaState) { /* Zilina external */
                    PlantCode = "7910", PlantName = "Zilina", PostalCode = "010 04",
                    Latitude = 49.194279, Longitude = 18.720064,
                    HasPaperMill = false,
                    FinishedGoodsWarehouseNames = new List<string> { "2100" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                },
                new Location(metsaState) { /* Krapkowice internal */
                    PlantCode = "4310", PlantName = "Krapkowice", PostalCode = "47-300",
                    Latitude = 50.4745052, Longitude = 17.9651385,
                    HasPaperMill = true,
                    BasepaperWarehouseNames = new List<string> {
                        "1010",
                        "1020",
                        "1110",
                        "1111",
                        "1140",
                        "1141",
                        "1210",
                        "1211", /* internal */
                        "1000"
                    },
                    FinishedGoodsWarehouseNames = new List<string> { "2200", "2500", /* internal */ "2000" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                    ConvertingLines = new[] {
                        "COMBO",
                        "L 17",
                        "SR 5",
                        "SR 7",
                        "SR 9",
                        "SR5",
                        "SR7",
                        "SR8",
                        "ZZ",
                        "ZZ 4",
                        "ZZ 5",
                        "ZZ4",
                        "ZZ5"
                    }.Select(name => new ConvertingLine(metsaState, name)).ToList()
                },
                new Location(metsaState) { /* Krapkowice external */
                    PlantCode = "4310", PlantName = "Krapkowice", PostalCode = "47-303",
                    Latitude = 50.478698, Longitude = 17.994525,
                    HasPaperMill = false,
                    FinishedGoodsWarehouseNames = new List<string> { "2101", /* internal */ "2001" },
                    FinishedGoodsWarehousesOverallCapacity = Tag.InfiniteFlow,
                }
            });

            var transportationCost = new TransportationCost { State = metsaState };
            metsaState.TransportationCost = transportationCost;

            metsaState.MatchFlows();

            metsaState.FinishedGoodsSplitter = new FinishedGoodsSplitter { State = metsaState };

            return metsaState;
        }

        public static MetsaState GetVariantAModel()
        {
            var metsaState = GetCurrentSetupModel();

            /* Line R10 moves from Kreuzau to Raubach */
            var kreuzauInternal =
                metsaState.Locations.Where(loc => loc.PlantCode == "9610" && loc.PostalCode == "52372").First();
            var raubachInternal =
                metsaState.Locations.Where(loc => loc.PlantCode == "9620" && loc.PostalCode == "56316").First();
            ConvertingLine r10Line = null;
            for (int i = 0; i < kreuzauInternal.ConvertingLines.Count; ++i)
            {
                if (kreuzauInternal.ConvertingLines[i].LineName == "R 10")
                {
                    r10Line = kreuzauInternal.ConvertingLines[i];
                    kreuzauInternal.ConvertingLines.RemoveAt(i);
                    break;
                }
            }
            raubachInternal.ConvertingLines.Add(r10Line);

            /*  Closure of Basepaper Storage Garnbleiche due to new on-site warehouse in Kreuzau */
            var garnbleiche = metsaState.Locations.First(loc => loc.PlantCode == "9610" && loc.PostalCode == "52353");
            garnbleiche.GetBasepaperWarehouse().Capacity = 0;
            kreuzauInternal.GetBasepaperWarehouse().Demand += garnbleiche.GetBasepaperWarehouse().Demand;
            garnbleiche.GetBasepaperWarehouse().Demand = 0;

            return metsaState;
        }

        public static void Main (string[] args)
        {

            // var metsaState = GetTheoreticalModel();
            var metsaState = GetCurrentSetupModel();

            var graph = metsaState.GenerateGraph ();
            
            /*Console.WriteLine("Visualizing...");
            GraphVisualizer.Visualize(graph.FlowGraph,true);
            Console.WriteLine("Visualization done");*/

            //graph.SolveUsingExternal ();
            graph.SolveWithFinishedGoodsWarehouseCapacity(1);

            metsaState.LoadFlowFromGraph(graph);

            /****************************
             *** Print internal flows ***
             ****************************/

            var output = new List<string>();
            output.Add(string.Join(";", new[] { "Ship-From Plant Code", "Ship-From Plant Name", "Ship-From PostalCode",
                "Ship-To Plant Code", "Ship-To Plant Name", "Ship-To PostalCode", "Material", "Is Basepaper", "Amount", "Amount Solved",
                "Transportation Cost Per Unit of Amount"
            }));

            foreach (var shipFrom in metsaState.Locations)
            {
                var shipFromDesc = string.Join(";", new[] { shipFrom.PlantCode, shipFrom.PlantName, shipFrom.PostalCode });

                foreach (var shipTo in metsaState.Locations)
                {
                    if (shipFrom == shipTo)
                        continue;

                    var shipToDesc = string.Join(";", new [] { shipTo.PlantCode, shipTo.PlantName, shipTo.PostalCode });

                    var commonMaterials = new HashSet<Material>(shipTo.FinishedGoodsWarehouses.Keys);
                    commonMaterials.IntersectWith(shipFrom.FinishedGoodsWarehouses.Keys);

                    if (shipFrom.HasBasepaperWarehouse && shipTo.HasBasepaperWarehouse)
                        commonMaterials.Add(metsaState.GetBasepaperMixMaterial());

                    foreach (var material in commonMaterials)
                    {
                        long amountLoaded = 0;
                        Dictionary<Material, long> materialDict = null;
                        if (shipFrom.OutFlow.TryGetValue(shipTo, out materialDict))
                            materialDict.TryGetValue(material, out amountLoaded);

                        long amountSolved = 0;
                        materialDict = null;
                        if (shipFrom.SolvedOutFlow.TryGetValue(shipTo, out materialDict))
                            materialDict.TryGetValue(material, out amountSolved);

                        if (amountLoaded == 0 && amountSolved == 0)
                            continue;

                        var c = metsaState.TransportationCost.InternalShipmentPerKg(shipFrom, shipTo, material);

                        var line = string.Join(";", new[] {
                            shipFromDesc,
                            shipToDesc,
                            material.MaterialName,
                            material.IsBasepaper.ToString (),
                            amountLoaded.ToString(),
                            amountSolved.ToString(),
                            c.ToString ()
                        });
                        output.Add(line);
                    }
                }
            }

            var tokyValidace = @"Toky_validace.csv";
            Console.WriteLine("Validace toku v souboru: {0}", tokyValidace);
            using (var outStream = new StreamWriter(tokyValidace))
            {
                foreach (var line in output)
                    outStream.WriteLine(line);
            }

            /****************************
             *** Print external flows ***
             ****************************/

            var outputZakaznici = new List<string>();
            outputZakaznici.Add(string.Join(";", new[] { "Ship-From Plant Code", "Ship-From Plant Name", "Ship-From PostalCode",
                "Ship-To", "Ship-To Customer name", "Ship-To Country", "Ship-To Postal Code", "Ship-To City of Ship-To Part", "Material", "Is Basepaper", "Amount", "Amount Solved", "Transportation Cost Per Unit of Amount"
            }));

            foreach (var shipFrom in metsaState.Locations)
            {
                var shipFromMaterials = new HashSet<Material>(shipFrom.FinishedGoodsWarehouses.Keys);
                if (shipFrom.HasBasepaperWarehouse)
                    shipFromMaterials.Add(metsaState.GetBasepaperMixMaterial());

                var shipFromDesc = string.Join(";", new[] { shipFrom.PlantCode, shipFrom.PlantName, shipFrom.PostalCode });


                foreach (var shipTo in metsaState.CustomersByShipTo.Values)
                {
                    var shipToDesc = string.Join(";", new string[] { shipTo.ShipTo, shipTo.CustomerName, shipTo.Country, shipTo.PostalCode, shipTo.CityOfShipToPart });

                    var commonMaterials = new HashSet<Material>(shipFromMaterials);
                    commonMaterials.IntersectWith(shipTo.MaterialNodes.Keys);

                    foreach (var material in commonMaterials)
                    {
                        long amountLoaded = 0;
                        Dictionary<Material, long> materialDict = null;
                        if (shipFrom.ToCustomerFlow.TryGetValue(shipTo, out materialDict))
                            materialDict.TryGetValue(material, out amountLoaded);

                        long amountSolved = 0;
                        materialDict = null;
                        if (shipFrom.SolvedToCustomerFlow.TryGetValue(shipTo, out materialDict))
                            materialDict.TryGetValue(material, out amountSolved);

                        if (amountLoaded == 0 && amountSolved == 0)
                            continue;

                        var c = metsaState.TransportationCost.ExternalShipmentPerKg(shipFrom, shipTo, material);

                        var line = string.Join(";", new[] {
                            shipFromDesc,
                            shipToDesc,
                            material.MaterialName,
                            material.IsBasepaper.ToString (),
                            amountLoaded.ToString(),
                            amountSolved.ToString(),
                            c.ToString()
                        });
                        outputZakaznici.Add(line);
                    }
                }

            }

            var tokyZakazniciValidace = @"Toky_Zakaznici_validace.csv";
            Console.WriteLine("Validace toku k zakaznikum v souboru: {0}", tokyZakazniciValidace);
            using (var outStream = new StreamWriter(tokyZakazniciValidace))
            {
                foreach (var line in outputZakaznici)
                    outStream.WriteLine(line);
            }




































            //var output = new List<string> ();
            //output.Add (string.Join (";", new[] { "Ship-From Plant Code", "Ship-From Plant Name", "Ship-From PostalCode",
            //    "Ship-To Plant Code", "Ship-To Plant Name", "Ship-To PostalCode", "Material", "Is Basepaper", "Amount", "Amount Solved",
            //    "Transportation Cost Per Unit of Amount"
            //}));
            //foreach (var loc in metsaState.Locations) {
            //    var shipFromDesc = string.Join (";", new [] { loc.PlantCode, loc.PlantName, loc.PostalCode });
            //    foreach (var shipTo in loc.OutFlow) 
            //    {
            //        var shipToDesc = string.Join (";", new string[] {
            //            shipTo.Key.PlantCode,
            //            shipTo.Key.PlantName,
            //            shipTo.Key.PostalCode
            //        });

            //        foreach (var kv in shipTo.Value) 
            //        {
            //            var c = metsaState.TransportationCost.InternalShipmentPerKg(loc, shipTo.Key, kv.Key);
            //            //var c = "Not Loading Now";
            //            string solvedFlow = "NULL";
            //            try { solvedFlow = loc.SolvedOutFlow[shipTo.Key][kv.Key].ToString(); } 
            //            catch { }
            //            var line = string.Join (";", new[] {
            //                shipFromDesc,
            //                shipToDesc,
            //                kv.Key.MaterialName,
            //                kv.Key.IsBasepaper.ToString (),
            //                kv.Value.ToString (),
            //                solvedFlow,
            //                c.ToString ()
            //            });
            //            output.Add (line);
            //        }
            //    }
            //}

            //var tokyValidace = @"Toky_validace.csv";
            //Console.WriteLine ("Validace toku v souboru: {0}", tokyValidace);
            //using (var outStream = new StreamWriter (tokyValidace)) {
            //    foreach (var line in output)
            //        outStream.WriteLine (line);
            //}

            //var outputZakaznici = new List<string> ();
            //outputZakaznici.Add (string.Join (";", new[] { "Ship-From Plant Code", "Ship-From Plant Name", "Ship-From PostalCode",
            //    "Ship-To", "Ship-To Customer name", "Ship-To Country", "Ship-To Postal Code", "Ship-To City of Ship-To Part", "Material", "Is Basepaper", "Amount", "Amount Solved", "Transportation Cost Per Unit of Amount"
            //}));
            //foreach (var loc in metsaState.Locations) {
            //    var shipFromDesc = string.Join (";", new [] { loc.PlantCode, loc.PlantName, loc.PostalCode });
            //    foreach (var shipToItem in loc.ToCustomerFlow)
            //    {
            //        var shipTo = shipToItem.Key;
            //        var shipToMaterials = shipToItem.Value;
            //        var shipToDesc = string.Join (";", new string[] {
            //            shipTo.ShipTo,
            //            shipTo.CustomerName,
            //            shipTo.Country,
            //            shipTo.PostalCode,
            //            shipTo.CityOfShipToPart
            //        });

            //        Dictionary<Material, long> shipToMaterialsSolved = null;
            //        loc.SolvedToCustomerFlow.TryGetValue(shipToItem.Key, out shipToMaterialsSolved);

            //        foreach (var materialItem in shipToMaterials)
            //        {
            //            var material = materialItem.Key;
            //            var amount = materialItem.Value;

            //            var c = metsaState.TransportationCost.ExternalShipmentPerKg(loc, shipTo, material);
            //            //var c = "Not Loading Now";

            //            long solvedFlow = -1;
            //            if (shipToMaterialsSolved != null)
            //            {
            //                if (!shipToMaterialsSolved.TryGetValue(materialItem.Key, out solvedFlow))
            //                    throw new Exception();
            //            } else throw new Exception();

            //            var line = string.Join (";", new[] {
            //                shipFromDesc,
            //                shipToDesc,
            //                material.MaterialName,
            //                material.IsBasepaper.ToString (),
            //                amount.ToString (),
            //                solvedFlow == -1 ? "NaN" : solvedFlow.ToString(),
            //                c.ToString ()
            //            });
            //            outputZakaznici.Add (line);
            //        }
            //    }
            //}

            //var tokyZakazniciValidace = @"Toky_Zakaznici_validace.csv";
            //Console.WriteLine ("Validace toku k zakaznikum v souboru: {0}", tokyZakazniciValidace);
            //using (var outStream = new StreamWriter (tokyZakazniciValidace)) {
            //    foreach (var line in outputZakaznici)
            //        outStream.WriteLine (line);
            //}

            Console.WriteLine (" *** SHIPMENT COSTS *** ");

            var totalShipmentCostFromData = metsaState.Flows.Rows.Select (r => r.ShipmentCost).Sum ();
            Console.WriteLine ("Total shipment cost: {0}", totalShipmentCostFromData);

            var totalCostInternal = 0.0;
            var totalCostExternal = 0.0;
            long totalKgs = 0;
            var totalCostByShipTo = metsaState.Locations.ToDictionary (loc => loc, loc => 0.0);
            var totalAmountByShipTo = metsaState.Locations.ToDictionary (loc => loc, loc => 0.0);
            foreach (var shipFrom in metsaState.Locations) {
                foreach (var outEdge in shipFrom.OutFlow) {
                    var shipTo = outEdge.Key;
                    foreach (var kv in outEdge.Value) {
                        totalKgs += kv.Value;
                        if (kv.Value == 0)
                            continue;

                        var costPerKg = metsaState.TransportationCost.InternalShipmentPerKg (shipFrom, shipTo, kv.Key);
                        var costIncrement = costPerKg * kv.Value;
                        totalCostInternal += costIncrement;
                        totalCostByShipTo [shipTo] += costIncrement;
                        totalAmountByShipTo [shipTo] += kv.Value;
                    }
                }
            }

            Console.WriteLine ("Total Kgs shipped: {0}", totalKgs);

            Console.WriteLine ("Total estimated shipment cost external: {0}", totalCostInternal);
            Console.WriteLine ("Total estimated shipment cost external: {0}", totalCostExternal);

            foreach (var kv in totalCostByShipTo) {
                Console.WriteLine ("Ship-To PlantCode: {0}, Total cost: {1}", kv.Key.PlantCode, kv.Value);
            }

            foreach (var kv in totalAmountByShipTo) {
                Console.WriteLine ("Ship-To PlantCode: {0} Ship-To PostalCode: {1}, Total amount: {2}", kv.Key.PlantCode, kv.Key.PostalCode, kv.Value);
            }

            var costDict = (from r in metsaState.Flows.Rows
                            group r by r.ShipToLocationLoaded into g
                            where g.Any ()
                            select g);
            foreach (var g in costDict) {
                if (g.Key == null)
                    continue;
                var blah = g.Any () ? g.Select (r => r.ShipmentCost).Sum ().ToString () : "No items";
                Console.WriteLine ("Ship-To: {0}, TotalCosts: {1}", g.Key.PlantCode, blah);
            }

            /************************naty testy***********************/
            
            if (false)
            {
                List<String> materialsNames = new List<String> {"200191", "200056", "214734","215240","213944",
                    "213631","204649","204650","215666","214878","214894","215348"};
                List<Material> materials_to_compute = new List<Material>();

                foreach (var mn in materialsNames)
                {
                    materials_to_compute.Add(metsaState.GetMaterial(mn));
                }

                System.IO.StreamWriter file = new System.IO.StreamWriter("costs.txt");
                file.WriteLine("Location code;Location postal code;Customer;Customer's region;Material;Basepaper;Cost");

                foreach (var location in metsaState.Locations)
                {
                    foreach (var c in metsaState.CustomersByShipTo.Values)
                    {
                        foreach (var m in materials_to_compute)
                        {
                            var computed_cost = metsaState.TransportationCost.ExternalShipmentPerKg(location, c, m);
                            file.WriteLine(location.PlantCode + ";" + location.PostalCode + ";" + c.ShipTo + ";" +
                                c.Region + ";" + m.MaterialName + ";" + m.IsBasepaper + ";" + computed_cost);
                        }

                    }
                }
                file.Close();
            }
            
        }
    }
}
