﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Metsa
{
    public class Geolocation
    {
        public MetsaState State;
        public List<string> Headers;
        public Dictionary<Tuple<string, string>, Tuple<double, double>> Geolocations = new Dictionary<Tuple<string, string>, Tuple<double, double>>();

        public bool TryGeolocate(string country, string postalCode, out double lat, out double lon)
        {
            var keyTuple = Tuple.Create(country, postalCode);
            Tuple<double, double> valueTuple;
            if (Geolocations.TryGetValue(keyTuple, out valueTuple))
            {
                lat = valueTuple.Item1;
                lon = valueTuple.Item2;
                return true;
            }
            else
            {
                lat = double.NaN;
                lon = double.NaN;
                return false;
            }
        }

        public int LoadFromFile(string fileName)
        {
            int lineNumber = 1;
            Headers = new List<string>();

            using (var inStream = new StreamReader(fileName))
            {
                var headersLine = inStream.ReadLine();

                Headers.AddRange(headersLine.Split(new[] { ';' }));
                if (Headers.Count != 4)
                    throw new InvalidDataException("Expected 4 columns in geolocations file");

                while (!inStream.EndOfStream)
                {
                    ++lineNumber;
                    var line = inStream.ReadLine();

                    var parts = line.Split(new[] { ';' });
                    if (parts.Length != 4)
                        continue;

                    double lat;
                    if (!double.TryParse(parts[2], out lat))
                        continue;

                    double lon;
                    if (!double.TryParse(parts[3], out lon))
                        continue;

                    var keyTuple = Tuple.Create(parts[0], parts[1]);
                    var valueTuple = Tuple.Create(lat, lon);
                    Geolocations.Add(keyTuple, valueTuple);
                }
            }

            Console.WriteLine("Parser {0} out of {1} lines (geolocation)", Geolocations.Count, lineNumber);
            return Geolocations.Count;
        }
    }
}
