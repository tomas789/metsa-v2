﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Metsa
{
    public class ConvertingLoader
    {
        public MetsaState State;
        public List<string> Headers;
        public Dictionary<string, List<Material>> Lines;

        public List<Material> GetMaterials(string[] lines)
        {
            var result = new HashSet<Material>();
            foreach (var line in lines)
                result.UnionWith(Lines[line]);
            return new List<Material>(result);
        }

        public int LoadFromFile(string fileName)
        {
            int lineNumber = 1;
            Lines = new Dictionary<string, List<Material>>();
            Headers = new List<string>();

            using (var inStream = new StreamReader(fileName))
            {
                var headersLine = inStream.ReadLine();

                Headers.AddRange(headersLine.Split(new[] { ';' }));
                if (Headers.Count != 6)
                    throw new InvalidDataException("Expected 15 columns in flows file");

                while (!inStream.EndOfStream)
                {
                    ++lineNumber;
                    var line = inStream.ReadLine();

                    var parts = line.Split(new[] { ';' });
                    if (parts.Length != 6)
                        continue;

                    if (!Lines.ContainsKey(parts[0]))
                        Lines.Add(parts[0], new List<Material>());

                    Lines[parts[0]].Add(State.GetMaterial(parts[1]));
                }
            }

            var loadedCount = (from kv in Lines
                               select kv.Value.Count).Sum();
            Console.WriteLine("Parser {0} out of {1} lines (converting)", loadedCount, lineNumber);

            return loadedCount;
        }
    }
}
