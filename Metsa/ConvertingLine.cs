﻿using System;
using System.Collections.Generic;
using QuickGraph;

namespace Metsa
{
    public class ConvertingLine : MetsaObjectWithState
    {
        public Location Location;
        public Node InputNode;
        public Node CapacityNode;
        public string LineName;

        public Dictionary<Material, Node> MaterialNodes; 

        public long Capacity = Tag.InfiniteFlow;

        public ConvertingLine(MetsaState state, string lineName)
            : base(state)
        {
            LineName = lineName;
            InputNode = new Node(state) {BelongsTo = this};
            CapacityNode = new Node(state) {BelongsTo = this};

            MaterialNodes = new Dictionary<Material, Node>();
            foreach (var material in State.ConvertingLoader.Lines[LineName])
                MaterialNodes.Add(material, new Node(State) {BelongsTo = this});
        }

        public void AddInternalsToGraph(Graph graph)
        {
            graph.FlowGraph.AddVertex(InputNode);
            graph.FlowGraph.AddVertex(CapacityNode);
            graph.FlowGraph.AddVertexRange(MaterialNodes.Values);
        }

        public void AddEdgesToGraph(Graph graph)
        {
            { /* Capacity edge */
                var tag = new Tag(State.GetBasepaperMixMaterial()) { TransportationCost = 0, UpperBound = Capacity };
                var edge = new TaggedEdge<Node, Tag>(InputNode, CapacityNode, tag);
                graph.FlowGraph.AddEdge(edge);
            }

            foreach (var materialNodeItem in MaterialNodes)
            {
                var material = materialNodeItem.Key;
                var materialNode = materialNodeItem.Value;

                var tag = new Tag(material) { TransportationCost = 0 };
                var edge = new TaggedEdge<Node, Tag>(CapacityNode, materialNode, tag);
                graph.FlowGraph.AddEdge(edge);
            }
        }
    }
}
