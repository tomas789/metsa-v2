﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metsa
{
    public class Material
    {
        public string MaterialName;
        public bool IsBasepaper { set { _isBasepaper = value; } get { return _isBasepaper; } }
        public bool IsFinishedGood { set { _isBasepaper = !value; } get { return !_isBasepaper; } }

        private bool _isBasepaper = false;
    }
}
