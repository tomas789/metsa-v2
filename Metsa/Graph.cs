﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using QuickGraph;

namespace Metsa
{
    public class Graph : MetsaObjectWithState
    {
        public static string GetPathByParts(params object[] parts) {
            return string.Join (Path.DirectorySeparatorChar.ToString (), parts.Select (p => p.ToString ()));
        }

        public Graph(MetsaState state) : base(state)
        {
        }

        static Graph() {
            var platformId = Environment.OSVersion.Platform;
            switch (platformId) {
            case PlatformID.MacOSX:
            case PlatformID.Unix:
                SolverExecutable = SolverExecutableOsX;
                break;
            case PlatformID.Win32NT:
            case PlatformID.Win32S:
            case PlatformID.Win32Windows:
            case PlatformID.WinCE:
                SolverExecutable = SolverExecutableWin32;
                break;
            }
        }

        public static readonly string SolverExecutableWin32 = GetPathByParts("solver", "dimacs-solver.exe"); 
        //public static readonly string SolverExecutableWin32 = GetPathByParts("solver", "cs2.exe"); 
        public static readonly string SolverExecutableOsX = GetPathByParts("solver", "dimacs-solver");

        public static string SolverExecutable;

        public AdjacencyGraph<Node, TaggedEdge<Node, Tag>> FlowGraph = new AdjacencyGraph<Node, TaggedEdge<Node, Tag>>();

        public void SolveWithFinishedGoodsWarehouseCapacity(int iterations)
        {
            SolveUsingExternal();
            State.FinishedGoodsSplitter.setCapacities(this);
            ClearSolvedValues();
            SolveUsingExternal();
        }

        public void ClearSolvedValues()
        {
            foreach (var edge in FlowGraph.Edges)
            {
                edge.Tag.SolvedFlow = 0;
            }
            foreach (var node in FlowGraph.Vertices)
            {
                node.SolvedDemand = 0;
            }
        }

        public void SolveUsingExternal()
        {
            using (var process = new Process())
            {
                process.StartInfo = new ProcessStartInfo
                {
                    FileName = SolverExecutable,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false
                };

                var standardOutputBuilder = new StringBuilder();
                var standardErrorBuilder = new StringBuilder();

                process.OutputDataReceived += (sender, args) =>
                {
                    if (args.Data != null)
                    {
                        standardOutputBuilder.AppendLine(args.Data);
                    }
                };

                process.ErrorDataReceived += (sender, args) =>
                {
                    if (args.Data != null)
                    {
                        standardErrorBuilder.AppendLine(args.Data);
                    }
                };

                process.Start();

                OffloadToSolver(process.StandardInput);
                process.StandardInput.Close();

                process.BeginOutputReadLine();
                process.BeginErrorReadLine();


                while (true)
                {
                    if (process.WaitForExit(2000))
                    {
                        Console.WriteLine("Sover finished!");
                        break;
                    }

                    Console.WriteLine("Still solving ...");
                }

                Console.WriteLine("Solver exit code: {0}", process.ExitCode);

                var standardOutput = standardOutputBuilder.ToString();
                var standardError = standardErrorBuilder.ToString();

                var standardErrorLines = standardError.Split(new [] {Environment.NewLine}, StringSplitOptions.None);
                foreach (var standardErrorLine in standardErrorLines)
                    Console.WriteLine("Solver stderr: {0}", standardErrorLine);

                OnloadFromSolver (new StringReader(standardOutput));
            }
        }

        public void OffloadToSolver(TextWriter solver)
        {
            HasParallelEdges();

            solver.WriteLine("p min {0} {1}", FlowGraph.VertexCount, FlowGraph.EdgeCount);

            foreach (var node in FlowGraph.Vertices)
                if (node.Demand != 0)
                    solver.WriteLine("n {0} {1}", node.Id + 1, node.Demand);

            foreach (var edge in FlowGraph.Edges)
                solver.WriteLine("a {0} {1} {2} {3} {4}", edge.Source.Id + 1, edge.Target.Id + 1, edge.Tag.LowerBound, edge.Tag.UpperBound, edge.Tag.SolverCost);

            solver.WriteLine();

            Console.WriteLine("Solver input generated.");
        }

        public void OnloadFromSolver(TextReader solver)
        {
            long solverCost = -1;
            string line;
            while ((line = solver.ReadLine()) != null)
            {
                var parts = line.Split(new [] {'\t', ' '}, StringSplitOptions.RemoveEmptyEntries);

                if (parts.Length == 0)
                    continue;

                switch (parts[0])
                {
                    case "c":
                        Console.WriteLine("Solver comment: {0}", string.Join(" ", parts));
                        break;
                    case "s":
                        solverCost = long.Parse(parts[1]);
                        break;
                    case "f":
                        var nodeIdFrom = int.Parse(parts[1]) - 1;
                        var nodeFrom = State.Nodes[nodeIdFrom];
                        var nodeIdTo = int.Parse(parts[2]) - 1;
                        var nodeTo = State.Nodes[nodeIdTo];
                        var loadedFlow = long.Parse(parts[3]);

                        var candidates = (FlowGraph.OutEdges(nodeFrom).Where(e => e.Target == nodeTo)).ToArray();

                        if (candidates.Length == 1)
                        {
                            var edge = FlowGraph.OutEdges(nodeFrom).First(e => e.Target == nodeTo);
                            edge.Tag.SolvedFlow += loadedFlow;
                            edge.Source.SolvedDemand += loadedFlow;
                            edge.Target.SolvedDemand -= loadedFlow;
                        }
                        else
                        {
                            throw new Exception(string.Format("Unexpected candidates count {0}", candidates.Length));
                        }

                        
                        break;
                }
            }

            foreach (var node in FlowGraph.Vertices)
            {
                if (node.Demand != node.SolvedDemand)
                    Console.WriteLine("Demand missmatch.");
            }

            Console.WriteLine("Solver cost: {0}", solverCost);
        }

        public bool HasParallelEdges()
        {
            foreach (var node in FlowGraph.Vertices)
            {
                var edges = new HashSet<Node>();

                foreach (var edge in FlowGraph.OutEdges(node))
                {
                    if (edges.Contains(edge.Target))
                        return true;

                    edges.Add(edge.Target);
                }
            }

            return false;
        }
    }
}
