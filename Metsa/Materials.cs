﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Metsa
{
    public class Materials
    {
        public MetsaState State;
        public List<string> Headers;
        public Dictionary<string, Material> Storage;

        public int LoadFromFile(string fileName)
        {
            State.GetBasepaperMixMaterial();

            if (State == null)
                throw new InvalidOperationException("MetsaState not loaded in Materials");

            var lineNumber = 1; /* one for header */
            Headers = new List<string>();

            using (var inStream = new StreamReader(fileName))
            {
                var headersLine = inStream.ReadLine();
                Headers.AddRange(headersLine.Split(new[] { ';' }));
                if (Headers.Count != 4)
                    throw new InvalidDataException("Expected 15 columns in materials file");

                while (!inStream.EndOfStream)
                {
                    ++lineNumber;
                    var line = inStream.ReadLine();
                    var parts = line.Split(new[] { ';' });
                    if (parts.Length != 4)
                        continue;

                    var material = State.GetMaterial(parts[0]);
                    material.IsBasepaper = true;
                }
            }

            var materialCount = State.MaterialsContainer.Count;
            Console.WriteLine("Parser {0} out of {1} lines (material)", materialCount, lineNumber);

            return materialCount;
        }
    }
}
