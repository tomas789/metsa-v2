﻿using System;
using System.Collections.Generic;
using QuickGraph;

namespace Metsa
{
    public class FinishedGoodsWarehouse : MetsaObjectWithState
    {
        public Material Material;

        public Node InputNode;
        public Node ExternalWarehouseNode;
        public Node OutputNode;

        public long Demand;

        public Location Location;
        public TaggedEdge<Node, Tag> CapacityEdge = null;

        public FinishedGoodsWarehouse(MetsaState state, Material material, Location location)
            : base(state)
        {
            InputNode = new Node(state) { BelongsTo = this };
            ExternalWarehouseNode = new Node(state) { BelongsTo = this };
            OutputNode = new Node(state) { BelongsTo = this };
            Material = material;
            Location = location;
        }

        public void AddInternalsToGraph(Graph graph)
        {
            graph.FlowGraph.AddVertex(InputNode);
            graph.FlowGraph.AddVertex(ExternalWarehouseNode);
            graph.FlowGraph.AddVertex(OutputNode);
        }

        public void AddEdgesToGraph(Graph graph)
        {
            { /* InputNode -> ExternalWarehouseNode */
                var tag = new Tag(Material) { TransportationCost = 0 };
                var edge = new TaggedEdge<Node, Tag>(InputNode, ExternalWarehouseNode, tag);
                graph.FlowGraph.AddEdge(edge);
            }

            { /* ExternalWarehouseNode -> OutputNode */
                var tag = new Tag(Material) { TransportationCost = 0 };
                var edge = new TaggedEdge<Node, Tag>(ExternalWarehouseNode, OutputNode, tag);
                CapacityEdge = edge;
                graph.FlowGraph.AddEdge(edge);
            }
        }

        public void InternalClearing()
        {
            if (Demand < 0)
            {
                ExternalWarehouseNode.Demand = Demand;
                InputNode.Demand = 0;
            }
            else
            {
                ExternalWarehouseNode.Demand = 0;
                InputNode.Demand = Demand;
            }
        }
    }
}
