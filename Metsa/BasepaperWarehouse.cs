﻿using System;
using QuickGraph;

namespace Metsa
{
    public class BasepaperWarehouse : MetsaObjectWithState
    {
        public Location Location;
        public Node InputNode;
        public Node OutputNode;
        public long Capacity = Tag.InfiniteFlow;

        public long Demand = 0;

        public BasepaperWarehouse(MetsaState state, Location location)
            : base(state)
        {
            Location = location;
            InputNode = new Node(state) {BelongsTo = this};
            OutputNode = new Node(state) {BelongsTo = this};
        }

        public void AddInternalsToGraph(Graph graph)
        {
            graph.FlowGraph.AddVertex(InputNode);
            graph.FlowGraph.AddVertex(OutputNode);
        }

        public void AddEdgesToGraph(Graph graph)
        {
            var tag = new Tag(State.GetBasepaperMixMaterial()) {TransportationCost = 0, UpperBound = Capacity};
            var edge = new TaggedEdge<Node, Tag>(InputNode, OutputNode, tag);
            graph.FlowGraph.AddEdge(edge);
        }

        public void InternalClearing()
        {
            if (Demand < 0)
            {
                OutputNode.Demand = Demand;
                InputNode.Demand = 0;
            }
            else
            {
                OutputNode.Demand = 0;
                InputNode.Demand = Demand;
            }
        }
    }
}
