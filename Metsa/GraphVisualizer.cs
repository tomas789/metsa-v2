﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickGraph.Graphviz;
using QuickGraph;
using QuickGraph.Graphviz.Dot;
using System.IO;

namespace Metsa
{
    class GraphVisualizer
    {
        public static void Visualize(AdjacencyGraph<Node, TaggedEdge<Node, Tag>> inputGraph, bool loadedState)
        {
            var graphviz =  GetSimpleGraph(inputGraph, loadedState); 

            string output = graphviz.Generate(new FileDotEngine(), "graph");
        }

        //returns simple graph with locations and customers
        private static GraphvizAlgorithm<Node, TaggedEdge<Node, VisualizationTag>> GetSimpleGraph(
            AdjacencyGraph<Node, TaggedEdge<Node, Tag>> originalGraph, bool loadedState)
        {
            var newGraph = new AdjacencyGraph<Node, TaggedEdge<Node, VisualizationTag>>();
            var addedLocations = new HashSet<Location>();
            var addedCustomerRegions = new HashSet<String>();
            var vertices = new Dictionary<object, Node>();

            foreach (var vertex in originalGraph.Vertices)
            {
                if (vertex.BelongsTo is Customer)
                {
                    var customer = (Customer) vertex.BelongsTo;
                    string region = customer.Region;
                    if (addedCustomerRegions.Contains(region)) continue;

                    newGraph.AddVertex(vertex);
                    addedCustomerRegions.Add(region);
                    vertices.Add(region,vertex);
                }
                else
                {
                    var location = vertex.tryGetLocation();
                    if (location == null) continue;
                    if (addedLocations.Contains(location)) continue;

                    newGraph.AddVertex(vertex);
                    addedLocations.Add(location);
                    vertices.Add(location, vertex);
                }     
            }

            var addedEdges = new Dictionary<Tuple<Node, Node>, TaggedEdge<Node, VisualizationTag>>();

            foreach (var edge in originalGraph.Edges)
            {
                if (edge.Source.BelongsTo == null || edge.Target.BelongsTo == null) continue;
                
                //get corresponding source and target node from new graph
                Node source = null;
                if (edge.Source.BelongsTo is Customer)
                    vertices.TryGetValue(((Customer) edge.Source.BelongsTo).Region, out source);
                else
                {
                    var location = edge.Source.tryGetLocation();
                    if (location != null) vertices.TryGetValue(location, out source);
                }
                Node target = null;
                if (edge.Target.BelongsTo is Customer)
                    vertices.TryGetValue(((Customer) edge.Target.BelongsTo).Region, out target);
                else
                {
                    var location = edge.Target.tryGetLocation();
                    if (location != null) vertices.TryGetValue(location, out target);
                }

                if (source == null || target == null || source == target) continue;

                TaggedEdge<Node, VisualizationTag> existingEdge;
                addedEdges.TryGetValue(Tuple.Create(source, target), out existingEdge);
                
                if (existingEdge == null)
                {
                    var newTag = new VisualizationTag
                    {
                        Flow = loadedState ? edge.Tag.MetsaFlow : edge.Tag.SolvedFlow,
                    };
  
                    var newEdge = new TaggedEdge<Node, VisualizationTag>(source, target, newTag);
                    newGraph.AddEdge(newEdge);
                    addedEdges.Add(Tuple.Create(source, target), newEdge);             
                }
                else
                {
                    existingEdge.Tag.Flow += loadedState ? edge.Tag.MetsaFlow : edge.Tag.SolvedFlow;
                }
            }



            var zeroFlow = new EdgePredicate<Node, TaggedEdge<Node, VisualizationTag>>(delegate(TaggedEdge<Node, VisualizationTag> te)
            {
                return te.Tag.Flow == 0;
            });

            newGraph.RemoveEdgeIf(zeroFlow);
            

            var graphviz = new GraphvizAlgorithm<Node, TaggedEdge<Node, VisualizationTag>>(newGraph);

            graphviz.FormatEdge +=
                delegate(object sender, FormatEdgeEventArgs<Node, TaggedEdge<Node, VisualizationTag>> e)
                {
                    e.EdgeFormatter.Label.Value = e.Edge.Tag.Flow.ToString();
                };
            graphviz.FormatVertex +=
                delegate(object sender, FormatVertexEventArgs<Node> v)
                {
                    string label = "";
                    GraphvizColor bgcolor;
                    GraphvizColor fgcolor;

                    if (v.Vertex.BelongsTo is Customer)
                    {
                        label = ((Customer) v.Vertex.BelongsTo).Region;
                       /* bgcolor = GraphvizColor.LightYellow;
                        fgcolor = GraphvizColor.Black;*/
                    }
                    else
                    {
                        Location location = v.Vertex.tryGetLocation();
                        if (location != null)
                            label = "Plant code: " + location.PlantCode + ", Postal code: " + location.PostalCode;
                        bgcolor = GraphvizColor.Black;
                        fgcolor = GraphvizColor.White;
                    }
                    v.VertexFormatter.Label = label;
                   /* v.VertexFormatter.FillColor = bgcolor;
                    v.VertexFormatter.FontColor = fgcolor;*/
                };


            return graphviz;
        }

    }

    

    public class FileDotEngine : IDotEngine
    {
        public string Run(GraphvizImageType imageType, string dot, string outputFileName)
        {
            string output = outputFileName;
            File.WriteAllText(output, dot);

            var args = string.Format(@"{0} -Tpng -O -Goverlap=prism", output);
            System.Diagnostics.Process.Start("Graphviz2.38\\bin\\sfdp.exe", args);
            return output;
        }
    }

    class VisualizationTag
    {
        public long Flow = 0;
    }
}
