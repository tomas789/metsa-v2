#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include <lemon/smart_graph.h>
#include <lemon/cost_scaling.h>
using namespace lemon;

using graph_type = SmartDigraph;
using value_type = long long;

std::vector<std::string> split(std::string s);

class MinCostFlowSolver {
    using graph_type = SmartDigraph;
    using node_type = graph_type::Node;
    using arc_type = graph_type::Arc;
    using solver_type = CostScaling<graph_type, long long>;

    std::unique_ptr<solver_type> solver_;
    solver_type::ProblemType result_;

    std::vector<node_type> nodes_;
    std::vector<std::tuple<std::size_t, std::size_t, arc_type>> arcs_;

    graph_type g_;

    graph_type::ArcMap<long long> lower_;
    graph_type::ArcMap<long long> cap_;
    graph_type::ArcMap<long long> cost_;
    graph_type::NodeMap<long long> sup_;

    void Split(std::string s, std::vector<std::string> &parts) const {
        std::string delimiter = " ";

        size_t pos = 0;
        std::string token;
        while ((pos = s.find(delimiter)) != std::string::npos) {
            token = s.substr(0, pos);
            parts.emplace_back(token);
            s.erase(0, pos + delimiter.length());
        }

        if (!s.empty())
            parts.emplace_back(s);
    }

    void ReadSolution(std::istream &in) {
        std::string setupLine;
        std::getline(in, setupLine);
        std::vector<std::string> setup;
        Split(setupLine, setup);

        auto node_count = std::stoul(setup[2]);
        auto arc_count = std::stoul(setup[3]);

        std::generate_n(std::back_inserter(nodes_), node_count, [&] { return g_.addNode(); });

        std::vector<std::string> parts;

        while (in) {
            std::string line;
            std::getline(in, line);

            if (line.empty())
                continue;

            parts.clear();
            Split(line, parts);

            if (parts[0] == "n") {
                auto nodeNumber = std::stoul(parts[1]);
                auto sup = std::stoll(parts[2]);
                auto node = nodes_[nodeNumber - 1];
                sup_[node] = sup;
            } else if (parts[0] == "a") {
                auto nodeFromNumber = std::stoul(parts[1]);
                auto nodeToNumber = std::stoul(parts[2]);
                auto lower = std::stoll(parts[3]);
                auto upper = std::stoll(parts[4]);
                auto cost = std::stoll(parts[5]);
                node_type nodeFrom = nodes_[nodeFromNumber - 1];
                node_type nodeTo = nodes_[nodeToNumber - 1];
                arc_type arc = g_.addArc(nodeFrom, nodeTo);
                arcs_.emplace_back(nodeFromNumber - 1, nodeToNumber - 1, arc);
                lower_[arc] = lower;
                cap_[arc] = upper;
                cost_[arc] = cost;
            }
        }

        if (nodes_.size() != node_count) 
            std::cerr << "Got " << nodes_.size() << " nodes, expected " << node_count << std::endl;

        if (arcs_.size() != arc_count)
            std::cerr << "Got " << arcs_.size() << " arcs, expected " << arc_count << std::endl;
    }

    void Solve() {
        solver_ = std::unique_ptr<solver_type>(new solver_type(g_));
        solver_->lowerMap(lower_);
        solver_->upperMap(cap_);
        solver_->costMap(cost_);
        solver_->supplyMap(sup_);

        result_ = solver_->run();
    }

    std::ostream &OutputSolution(std::ostream &out) {
        if (result_ == solver_type::ProblemType::INFEASIBLE) {
            out << "s INFEASIBLE" << std::endl;
        } else if (result_ == solver_type::ProblemType::UNBOUNDED) {
            out << "s UNBOUNDED" << std::endl;
        } else {
            out << "s " << solver_->totalCost() << std::endl;

            for (std::size_t i = 0; i < arcs_.size(); ++i)
                out << "f " 
                    << std::get<0>(arcs_[i]) + 1 << " "
                    << std::get<1>(arcs_[i]) + 1 << " "
                    << solver_->flow(std::get<2>(arcs_[i]))
                    << std::endl;


            for (std::size_t i = 0; i < nodes_.size(); ++i)
                out << "p " << (i + 1) << " " << solver_->potential(nodes_[i]) << std::endl;
        }

        return out;
    }
public:
    MinCostFlowSolver()
      : g_(), lower_(g_), cap_(g_), cost_(g_), sup_(g_) { }

    std::ostream &Solve(std::ostream &out, std::istream &in) {
        ReadSolution(in);
        Solve();
        return OutputSolution(out);
    }
};







int
main(int argc, char * argv[]) {
    MinCostFlowSolver solver;
    solver.Solve(std::cout, std::cin);
    return 0;
}
